<?php
/**
 * @file
 * Drupal Module: SiteCatalyst Stats
 * Adds the required Javascript to the bottom of your Drupal pages to allow
 * tracking by the SiteCatalyst analytics package. The original code for this
 * module came from the Omniture module which was based on Google Analytics
 * package by Mike Carter.
 *
 * @author: Alexander Ross (bleen18)
 */

define('SITECATALYST_TOKEN_CACHE', 'sitecatalyst:tag_token_results');

/**
 * Implements hook_help().
 */
function sitecatalyst_help($path, $arg) {
  switch ($path) {
    case 'admin/help#sitecatalyst':
      return t("Settings for SiteCatalyst analytics.");
  }
}

/**
 * Implements hook_permission().
 */
function sitecatalyst_permission() {
  return array(
    'administer SiteCatalyst configuration' => array(
      'title' => t('Administer SiteCatalyst configuration'),
    )
  );
}

/**
 * Implements hook_menu().
 */
function sitecatalyst_menu() {
  $items['admin/config/system/sitecatalyst'] = array(
    'title' => 'SiteCatalyst',
    'description' => "Configure the settings used to integrate SiteCatalyst analytics.",
    'page callback' => 'drupal_get_form',
    'page arguments' => array('sitecatalyst_admin_settings'),
    'access arguments' => array('administer SiteCatalyst configuration'),
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}

/**
 * Implements hook_theme().
 */
function sitecatalyst_theme($existing, $type, $theme, $path) {
  return array(
    'sitecatalyst_variables' => array(
      'render element' => 'form',
    ),
  );
}

/**
 * Implements hook_context_plugins().
 */
function sitecatalyst_context_plugins() {
  $plugins = array();
  $plugins['sitecatalyst_reaction_variables'] = array(
    'handler' => array(
      'path' => drupal_get_path('module', 'sitecatalyst') .'/plugins/context',
      'file' => 'sitecatalyst_reaction_variables.inc',
      'class' => 'sitecatalyst_reaction_variables',
      'parent' => 'context_reaction',
    ),
  );
  return $plugins;
}

/**
 * Implements hook_context_registry().
 */
function sitecatalyst_context_registry() {
  $reg = array(
    'reactions' => array(
      'sitecatalyst_vars' => array(
        'title' => t('SiteCatalyst Variables'),
        'plugin' => 'sitecatalyst_reaction_variables',
      ),
    ),
  );
  return $reg;
}

/**
 * Implementation of hook_sitecatalyst_variables().
 */
function sitecatalyst_sitecatalyst_variables() {
  $variables = array();

  // Include variables set using the context module.
  if (module_exists('context')) {
    if ($plugin = context_get_plugin('reaction', 'sitecatalyst_vars')) {
      $plugin->execute($variables);
    }
  }

  // Include variables from the "custom variables" section of the settings form.
  $settings_variables = variable_get('sitecatalyst_variables', array());
  foreach ($settings_variables as $variable) {
    $variables[$variable['name']] = $variable['value'];
  }

  return array('variables' => $variables);
}

/**
 * Implements hook_page_alter().
 */
function sitecatalyst_page_alter(&$page) {
  global $user;

  // Check if we should track the currently active user's role.
  $track = 0;
  if (is_array($user->roles)) {
    foreach ($user->roles as $role) {
      $role = str_replace(' ', '_', $role);
      $track += variable_get("sitecatalyst_track_{$role}", FALSE);
    }
  }

  $tracking_type = variable_get('sitecatalyst_role_tracking_type', 'inclusive');
  $track = $tracking_type == 'inclusive' ? $track > 0 : $track <= 0;

  // Don't track page views in the admin sections, or for certain roles.
  if (path_is_admin(current_path()) || !$track) {
    return;
  }

  // Like drupal_add_js, add a query string to the end of the js file location.
  $query_string = '?' . variable_get('css_js_query_string', '0');
  $js_file_location = check_plain(variable_get("sitecatalyst_js_file_location", '//www.example.com/js/s_code_remote_h.js'));

  // Add any custom code snippets if specified and replace any tokens.
  $context = _sitecatalyst_get_token_context();
  $codesnippet = sitecatalyst_token_replace(variable_get('sitecatalyst_codesnippet', ''), $context, array(
    'clear' => TRUE,
    'sanitize' => TRUE,
  )) . "\n";

  // Format and combine variables in the "right" order
  // Right order is the code file (list likely to be maintained)
  // Then admin settings with codesnippet first and finally taxonomy->vars
  $extra_variables_formatted = $codesnippet;

  $header = "<!-- SiteCatalyst code version: ";
  $header .= check_plain(variable_get("sitecatalyst_version", 'H.20.3.'));
  $header .= "\nCopyright 1996-" . date('Y') . " Adobe, Inc. -->\n";
  $header .= "<script type=\"text/javascript\" src=\"";
  $header .= $js_file_location . $query_string;
  $header .= "\"></script>\n";
  $header .= "<script type=\"text/javascript\"><!--\n";

  $footer = '/************* DO NOT ALTER ANYTHING BELOW THIS LINE ! **************/'."\n";
  $footer .= 'var s_code=s.t();if(s_code)document.write(s_code)//--></script>'."\n";
  $footer .= '<script type="text/javascript"><!--'."\n";
  $footer .= "if(navigator.appVersion.indexOf('MSIE')>=0)document.write(unescape('%3C')+'\!-'+'-')"."\n";
  $footer .= '//--></script>' . "\n";
  $nojs = variable_get("sitecatalyst_image_file_location", 'http://examplecom.112.2O7.net/b/ss/examplecom/1/H.20.3--NS/0');
  if (!empty($nojs)) {
    $footer .= '<noscript><img src="' . check_url($nojs . '/' . rand(0, 10000000)) . '" height="1" width="1" alt=""></noscript>' . "\n";
  }
  $footer .= '<!--/DO NOT REMOVE/-->' . "\n";
  $footer .= '<!-- End SiteCatalyst code version: ';
  $footer .=  check_plain(variable_get("sitecatalyst_version", 'H.20.3.'));
  $footer .= ' -->'."\n";

  $header = '';
  $footer = '';
  drupal_add_js($js_file_location, array('scope' => 'header'));

  if ($sitecatalyst_hooked_vars = module_invoke_all('sitecatalyst_variables', $page)) {
    if (isset($sitecatalyst_hooked_vars['header'])) {
      $header = $sitecatalyst_hooked_vars['header'];
    }
    if (isset($sitecatalyst_hooked_vars['variables'])) {
      $extra_variables_formatted .= _sitecatalyst_format_variables($sitecatalyst_hooked_vars['variables']);
    }
    if (isset($sitecatalyst_hooked_vars['footer'])) {
      $footer = $sitecatalyst_hooked_vars['footer'];
    }
  }

  $extra_variables_formatted = '<script type="text/javascript">_satellite.pageBottom();</script>';

  $page['page_bottom']['sitecatalyst'] =  array(
    'header'=> array(
      '#type' => 'markup',
      '#markup' => $header,
    ),
    'variables'=> array(
      '#type' => 'markup',
      '#markup' => $extra_variables_formatted,
    ),
    'footer'=> array(
      '#type' => 'markup',
      '#markup' => $footer,
    ),
  );

  // Set adobe dtm params for new changes
  $params = [];
  $fields = [];
  $view = views_get_page_view();

  if (strpos(current_path(), 'node') !== false) {
    $node_id = basename(current_path());
    $node = node_load($node_id);

    $params['create_date'] = format_date($node->created, 'custom', 'Y-m-d\TH:i:s');
    $params['update_date'] = format_date($node->changed, 'custom', 'Y-m-d\TH:i:s');
    $params['content_type'] = $node->type;
    $current_page = field_info_instances("node",'landing');
    foreach($current_page as $field_name => $field) {
      $fields[] = $field_name;
    }
    $params['fields'] = (count($fields) ? implode('|', $fields): '');
  } else if(isset($view)) {
    $params['content_type'] = 'view';
  }
  $experience_section = variable_get('adobe_dtm_param_experience_section');
  $params['business_unit'] = variable_get('adobe_dtm_param_business_unit');
  $params['experience'] = variable_get('adobe_dtm_param_experience');
  $params['experience_section'] = isset($experience_section) ? $experience_section : '';
  $settings = array(
    'sitecatalyst' => $params
  );
  drupal_add_js($settings, 'setting');
  drupal_add_js(drupal_get_path('module', 'sitecatalyst').'/js/sitecatalyst.js', array('scope' => 'header'));

}

/**
 * Menu callback; SiteCatalyst settings.
 */
function sitecatalyst_admin_settings($form, &$form_state) {
  $form['general'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
    '#collapsible' => TRUE,
    '#weight' => '-10',
  );

  $form['general']['sitecatalyst_js_file_location'] = array(
    '#type' => 'textfield',
    '#title' => t("Complete path to SiteCatalyst Javascript file"),
    '#default_value' => variable_get("sitecatalyst_js_file_location", 'http://www.example.com/js/s_code_remote_h.js'),
  );

  $form['general']['sitecatalyst_image_file_location'] = array(
    '#type' => 'textfield',
    '#title' => t("Complete path to SiteCatalyst Image file"),
    '#default_value' => variable_get("sitecatalyst_image_file_location", 'http://examplecom.112.2O7.net/b/ss/examplecom/1/H.20.3--NS/0'),
  );

  $form['general']['sitecatalyst_version'] = array(
    '#type' => 'textfield',
    '#title' => t("SiteCatalyst version (used by sitecatalyst for debugging)"),
    '#default_value' => variable_get("sitecatalyst_version", 'H.20.3.'),
  );

  $form['general']['sitecatalyst_token_cache_lifetime'] = array(
    '#type' => 'textfield',
    '#title' => t("Token cache lifetime"),
    '#default_value' => variable_get("sitecatalyst_token_cache_lifetime", 0),
    '#description' => t('The time, in seconds, that the SiteCatalyst token cache will be valid for. The token cache will always be cleared at the next system cron run after this time period, or when this form is saved.')
  );

  $form['general']['adobe_dtm_param_business_unit'] = array(
    '#type' => 'textfield',
    '#title' => t('Adobe DTM Tag Business Unit'),
    '#default_value' => variable_get('adobe_dtm_param_business_unit', 'education_research'),
  );
  $form['general']['adobe_dtm_param_experience'] = array(
    '#type' => 'textfield',
    '#title' => t('Adobe DTM Tag Experience'),
    '#default_value' => variable_get('adobe_dtm_param_experience', 'nyu_langone_med_web'),
  );
  $form['general']['adobe_dtm_param_experience_section'] = array(
    '#type' => 'textfield',
    '#title' => t('Adobe DTM Tag Experience Section'),
    '#default_value' => variable_get('adobe_dtm_param_experience_section', ''),
  );
  
  $form['roles'] = array(
    '#type' => 'fieldset',
    '#title' => t('User role tracking'),
    '#collapsible' => TRUE,
    '#description' => t('Define which user roles should, or should not be tracked by SiteCatalyst.'),
    '#weight' => '-6',
  );

  $form['roles']['sitecatalyst_role_tracking_type'] = array(
    '#type' => 'select',
    '#title' => t('Add tracking for specific roles'),
    '#options' => array(
      'inclusive' => t('Add to the selected roles only'),
      'exclusive' => t('Add to all roles except the ones selected'),
    ),
    '#default_value' => variable_get("sitecatalyst_role_tracking_type", 'inclusive'),
  );

  $result = db_select('role','r')
    ->fields('r')
    ->orderBy('name', 'ASC')
    ->execute();

  foreach ($result as $role) {
    // Can't use empty spaces in varname.
    $role_varname = str_replace(' ', '_', $role->name);
    // Only the basic roles are translated.
    $role_name = in_array($role->rid, array(DRUPAL_ANONYMOUS_RID, DRUPAL_AUTHENTICATED_RID)) ? t($role->name) : $role->name;
    $form['roles']["sitecatalyst_track_{$role_varname}"] = array(
      '#type' => 'checkbox',
      '#title' => $role_name,
      '#default_value' => variable_get("sitecatalyst_track_{$role_varname}", FALSE),
    );
  }

  $form['variables'] = array(
    '#type' => 'fieldset',
    '#title' => t('Custom Variables'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t('You can define tracking variables here.'),
    '#weight' => '-3',
  );

  $existing_variables = isset($form_state['input']['sitecatalyst_variables']) ? $form_state['input']['sitecatalyst_variables'] : variable_get('sitecatalyst_variables', array());
  _sitecatalyst_variables_form($form['variables'], $existing_variables);


  $form['advanced'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t('You can add custom SiteCatalyst code here.'),
    '#weight' => '-2',
  );
  $examples = array(
    'if ([current-date:custom:N] >= 6) { s.prop5 = "weekend"; }',
    'if ("[current-page:url:path]" == "node") {s.prop9 = "homepage";} else {s.prop9 = "[current-page:title]";}',
  );
  $form['advanced']['sitecatalyst_codesnippet'] = array(
    '#type' => 'textarea',
    '#title' => t('JavaScript Code'),
    '#default_value' => variable_get('sitecatalyst_codesnippet', ''),
    '#rows' => 15,
    '#description' => t('Examples:') . theme('item_list', array('items' => $examples)),
  );
  $form['advanced']['tokens'] = array(
    '#theme' => 'token_tree',
    '#token_types' => array('node', 'menu', 'term', 'user'),
    '#global_types' => TRUE,
    '#click_insert' => TRUE,
    '#dialog' => TRUE,
  );

  return system_settings_form($form);
}

/**
 * Validation function used by the variables form.
 */
function sitecatalyst_variables_form_validate($form, &$form_state) {
  if ($form_state['triggering_element']['#value'] != t('Add another variable')) {
    sitecatalyst_variables_trim_empties($form_state['values']);
  }
}

/**
 * Submit function for the variables form.
 */
function sitecatalyst_variables_form_submit($form, &$form_state) {
  // clear our cached token generation, since it may have just changed anyway.
  cache_clear_all(SITECATALYST_TOKEN_CACHE, 'cache', TRUE);
}

/**
 * Given the values entered into the sitecatalyst variables form, remove any empty
 * variables (i.e. both "name" & "value" are blank).
 */
function sitecatalyst_variables_trim_empties(&$values, $parent = 'sitecatalyst_variables') {
  foreach ($values as $key => &$val) {
    if ($key === $parent) {
      // We found the sitecatalyst variables.
      foreach ($val as $k => $v) {
        if (empty($val[$k]['name']) && empty($val[$k]['value'])) {
          unset($val[$k]);
        }
      }
      // Reset the array indexes to prevent wierd behavior caused by a variable
      // being removed in the middle of the array.
      $val = array_values($val);
      break;
    }
    elseif (is_array($val)) {
      sitecatalyst_variables_trim_empties($val, $parent);
    }
  }
}

/**
 * AJAX callback function for adding variable fields to the settings form.
 */
function sitecatalyst_add_another_variable_js($form, $form_state) {
  // @todo By hard-coding, "variables" here it forces a generic name for the containing form element. This is awkward for the node edit form.
  return $form['variables']['sitecatalyst_variables'];
}

/**
 * Submit handler to add more variables.
 */
function sitecatalyst_add_another_variable_submit($form, &$form_state) {
  $form_state['sitecatalyst_variables'] = $form_state['input']['sitecatalyst_variables'];
  $form_state['rebuild'] = TRUE;
}

/**
 * Validation function for variable names.
 */
function sitecatalyst_validate_variable_name($element, &$form_state, $form) {
  // Variable names must follow the rules defined by javascript syntax.
  if (!empty($element['#value']) && !preg_match("/^[A-Za-z_$]{1}\S*$/", $element['#value'])) {
    form_error($element, t('This is not a valid variable name. It must start with a letter, $ or _ and cannot contain spaces.'));
  }
}

/**
 * Form validation.
 */
function sitecatalyst_admin_settings_validate($form, &$form_state) {
  // Remove any empty variables.
  foreach ($form_state['values']['sitecatalyst_variables'] as $key => $val) {
    if (empty($val['name']) && empty($val['value'])) {
      unset($form_state['values']['sitecatalyst_variables'][$key]);
    }
  }
  $form_state['values']['sitecatalyst_variables'] = array_values($form_state['values']['sitecatalyst_variables']);
}

/**
 * Used to replace the value of SiteCatalyst variables. The variables need to be
 * defined with hook_sitecatalyst_variables().
 *
 * @param string $name
 *  The property.
 *
 * @param string $value
 *  The value for the property.
 */
function sitecatalyst_set_variable($name = NULL, $value = NULL) {
  $variables = &drupal_static(__FUNCTION__, array());

  if (empty($name)) {
    return $variables;
  }
  else {
    $variables[$name] = $value;
  }
}

function sitecatalyst_get_variables() {
  return sitecatalyst_set_variable();
}

/**
 * Theme function for the "variables" form.
 */
function theme_sitecatalyst_variables($variables) {
  $form = $variables['form'];

  $add_button = drupal_render($form['add_another_variable']);
  unset($form['add_another_variable']);

  $headers = array(t('Name'), t('Value'));
  $rows = array();
  foreach (element_children($form) as $key) {
    $rows[] = array(drupal_render($form[$key]['name']), drupal_render($form[$key]['value']));
  }

  return theme('table', array('header' => $headers, 'rows' => $rows)) . $add_button;
}

/**
 * Helper function to format SiteCatalyst variables.
 */
function _sitecatalyst_format_variables(array $variables = array()) {
  $extra_variables = sitecatalyst_get_variables();

  // Create context data to be used by token.
  $context = !empty($variables) ? _sitecatalyst_get_token_context() : array();

  $variables_formatted = '';
  foreach ($variables as $key => $value) {
    if (is_array($value)) {
      // Use the last element.
      $value = end($value);
    }

    if (isset($extra_variables[$key])) {
      $value = $extra_variables[$key];
    }

    // Cannot use check_plain() here because $key may contain quotes (e.g. 's.contextData["tve_domain"]').
    $key = htmlspecialchars($key, ENT_NOQUOTES, 'UTF-8');
    $value = sitecatalyst_token_replace($value, $context, array(
      'clear' => TRUE,
      'sanitize' => FALSE,
    ));
    $value = drupal_json_encode($value);
    $variables_formatted .= "{$key}={$value};\n";
  }
  return $variables_formatted;
}


/**
 * Replaces all tokens in a given string with appropriate values, with caching.
 *
 * This function is a memoizing wrapper for token_replace(), which is quite slow
 * and inefficient.  It takes advantage of specific knowledge about how this
 * module works to cache the result of token replacement. It is not a fully
 * general solution but works for this module.
 *
 * @param string $text
 *   A string potentially containing replaceable tokens.
 * @param $data
 *   (optional) An array of keyed objects. See token_replace(). Known objects
 *   here will also be used to form the cache key.
 * @param array $options
 *   An array of options to pass to token_replace().  See that function for
 *   further documentation.
 * @return string
 *   Text with tokens replaced.
 *
 * @see token_replace()
 */
function sitecatalyst_token_replace($text, $data = array(), array $options = array()) {
  $processed_strings =& drupal_static(__FUNCTION__, NULL);

  // Short-circuit the degenerate case, just like token_replace() does.
  $text_tokens = token_scan($text);
  if (empty($text_tokens)) {
    return $text;
  }

  // Determine the cache key for this text string. That way we can cache reliably.
  $key = _sitecatalyst_token_replace_make_key($text, $data);

  $cache_item = SITECATALYST_TOKEN_CACHE . ':' . current_path();

  // Lookup any already-cached token replacements.
  if (is_null($processed_strings)) {
    $cache = cache_get($cache_item, 'cache');
    $processed_strings = $cache
      ? $cache->data
      : array();
  }

  // If the processed string we're looking for isn't already in the cache,
  // then, and only then, do we call the expensive token_replace() (and cache
  // the result).
  if (!isset($processed_strings[$key]) || is_null($processed_strings[$key])) {
    // Regenerate this particular replacement.
    $processed_strings[$key] = token_replace($text, $data, $options);
    $lifetime = variable_get('sitecatalyst_token_cache_lifetime', 0);
    $expire_at = ($lifetime == 0) ? CACHE_TEMPORARY : (REQUEST_TIME + $lifetime);
    cache_set($cache_item, $processed_strings, 'cache', $expire_at);
  }

  return $processed_strings[$key];
}

/**
 * Generates an identifying key for the lookup to be processed.
 *
 * @param string $text
 *   The text to be processed.
 * @param array $data
 *   The array of data parameters that will be passed to token_generate().
 *   We'll use knowledge of what is expected in that array to build a
 *   meaningful lookup key.
 * @return string
 *   The key in the lookup array that corresponds to this tokenization request.
 */
function _sitecatalyst_token_replace_make_key($text, array $data) {

  // $text may be arbitrarily long, which can slow-down lookups. Hashing it
  // keeps uniqueness but guarantees a manageable size. Since this value won't
  // be used as the cache key itself we're not limited to 255 characters but
  // it will be nicer on array lookups in PHP.
  $keys[] = sha1($text);
  $keys[] = isset($data['node']->nid) ? $data['node']->nid . '-' . entity_modified_last('node', $data['node'])  : NULL;
  $keys[] = isset($data['menu']->menu_name) ? $data['menu']->menu_name . '-' . entity_modified_last('menu', $data['menu']) : NULL;
  $keys[] = isset($data['tag']->machinename) ? $data['tag']->machinename . '-' . entity_modified_last('tag', $data['tag']) : NULL;

  return implode('|', array_filter($keys));
}

/**
 * Helper form builder for a variables form.
 */
function _sitecatalyst_variables_form(&$form, $existing_variables = array()) {
  $form['sitecatalyst_variables'] = array(
    '#type' => 'markup',
    '#tree' => FALSE,
    '#prefix' => '<div id="sitecatalyst-variables-wrapper">',
    '#suffix' => '</div>',
    '#theme' => 'sitecatalyst_variables',
    '#element_validate' => array('sitecatalyst_variables_form_validate'),
  );
  // Add existing variables to the form unless they are empty.
  foreach ($existing_variables as $key => $data) {
    _sitecatalyst_variable_form($form, $key, $data);
  }
  // Add one blank set of variable fields.
  _sitecatalyst_variable_form($form, count($existing_variables));
  $form['add_another_variable'] = array(
    '#type' => 'submit',
    '#value' => t('Add another variable'),
    '#submit' => array('sitecatalyst_add_another_variable_submit'),
    '#limit_validation_errors' => array(),
    '#ajax' => array(
      'callback' => 'sitecatalyst_add_another_variable_js',
      'wrapper' => 'sitecatalyst-variables-wrapper',
      'effect' => 'fade',
    ),
  );
  $form['tokens'] = array(
    '#theme' => 'token_tree',
    '#token_types' => array('node', 'menu', 'term', 'user'),
    '#global_types' => TRUE,
    '#click_insert' => TRUE,
    '#dialog' => TRUE,
  );
}

/**
 * Helper form builder for an individual variable.
 */
function _sitecatalyst_variable_form(&$form, $key, $data = array()) {
  $form['sitecatalyst_variables'][$key]['name'] = array(
    '#type' => 'textfield',
    '#title_display' => 'invisible',
    '#title' => t('Name'),
    '#default_value' => isset($data['name']) ? $data['name'] : '',
    '#parents' => array('sitecatalyst_variables', $key, 'name'),
    '#attributes' => array('class' => array('field-variable-name')),
    '#element_validate' => array('sitecatalyst_validate_variable_name'),
  );
  $form['sitecatalyst_variables'][$key]['value'] = array(
    '#type' => 'textarea',
    '#rows' => 1,
    '#title_display' => 'invisible',
    '#title' => t('Value'),
    '#default_value' => isset($data['value']) ? $data['value'] : '',
    '#parents' => array('sitecatalyst_variables', $key, 'value'),
    '#attributes' => array('class' => array('field-variable-value')),
  );
  if (empty($data)) {
    $form['sitecatalyst_variables'][$key]['name']['#description'] = t('Example: prop1');
    $form['sitecatalyst_variables'][$key]['value']['#description'] = t('Example: [current-page:title]');
  }
}

/**
 * Create a data array to be used by token_replace().
 *
 * @return array
 */
function _sitecatalyst_get_token_context() {
  $context = &drupal_static(__function__);

  if (is_null($context)) {
    $context['node'] = menu_get_object('node');
    $context['term'] = menu_get_object('taxonomy_term', 2);
    if (module_exists('menu')) {
      $context['menu'] = menu_load('main-menu');
    }
  }

  return $context;
}
