(function($) {
    Drupal.behaviors.sitecatalyst = {
        attach: function (context, settings) {
            window.nyu_med_data = {};
            window.nyu_med_data.global = {}
            window.nyu_med_data.global.brand = "nyu_langone";
            window.nyu_med_data.global.business_unit = Drupal.settings.sitecatalyst.business_unit;
            window.nyu_med_data.global.experience = Drupal.settings.sitecatalyst.experience;
            window.nyu_med_data.global.experience_section = Drupal.settings.sitecatalyst.experience_section;
            window.nyu_med_data.global.page_type = "";
            window.nyu_med_data.global.page_id = "";
            window.nyu_med_data.global.modules = Drupal.settings.sitecatalyst.content_type;
            window.nyu_med_data.global.fields = Drupal.settings.sitecatalyst.fields;
            window.nyu_med_data.global.publish_date = Drupal.settings.sitecatalyst.create_date;
            window.nyu_med_data.global.modified_date = Drupal.settings.sitecatalyst.update_date;

        }
    }
})(jQuery);